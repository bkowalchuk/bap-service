﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace BAPService
{

    class CompletionMessage
    {
        public CompletionMessage()
        {
            FilesCreated = new StringCollection();
        }

        public string Subject { get; set; }
        public int InvoiceID { get; set; }
        public string FileName { get; set; }
        public string InvoiceNumber { get; set; }
        public int RecordsLoadedCount { get; set; }
        public int ExceptionCount { get; set; }
        public StringCollection FilesCreated { get; set; }

    }
}
