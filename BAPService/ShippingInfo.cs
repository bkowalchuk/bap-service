﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAPService
{
    class ShippingInfo
    {
        public string UserName {get; set;}
        public string Password { get; set; }
        public string ClientID { get; set; }
        public string OrderNumber { get; set; }
        public string OriginCity { get; set; }
        public string OriginProvince { get; set; }
        public string OriginPostalCode { get; set; }
        public string OriginCountryCode { get; set; }
        public string ConsigneeName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public int NumOfPackages { get; set; }
        public float TotalWeight { get; set; }
        public string ReferenceNumber { get; set; }
        public int ServiceType { get; set; }
        public int RecordNumber {get; set;}
    }
}
