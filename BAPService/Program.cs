﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic.FileIO;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.Net.Mail;

namespace BAPService
{
    class Program
    {
        private static ConsoleColor ScreenBackColor = ConsoleColor.DarkBlue;

        private static string BilsiClientID = "BILSI";
        private static string BilsiUsername = "12345";
        private static string BilsiPassword = "password";

        private static int ExceptionCount;
        private static string InvoiceNumber;
        private static CompletionMessage CompMessage;
        private static DateTime AppStartTime;


        static void Main(string[] args)
        {

            Console.Title = "BAP File Watcher";
            Console.BackgroundColor = ScreenBackColor;
            Console.Clear();

            int repostinvoiceid = 0;

            //Check the command line args
 

            CompMessage = new CompletionMessage();

            string border = new string('═', 77);

            System.Reflection.Assembly executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            var fieVersionInfo = FileVersionInfo.GetVersionInfo(executingAssembly.Location);
            var version = fieVersionInfo.FileVersion;


            Console.WriteLine("╒" + border + "╕");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║                     ◄► Billing Analysis Process Watcher ◄►                  ║");
            Console.WriteLine("║                                 Version " + version + "                             ║");
            Console.WriteLine("║                                                                             ║");

            Console.WriteLine("╘" + border + "╛");

            //Console.Clear();
            //Console.WriteLine("► Error Happened");
            Console.BackgroundColor = ScreenBackColor;

            //Get options from command line
            if (args.Length > 0)
            {
                string[] repost = args[0].Split(':');
                if (repost[0] == "repost")
                {
                    repostinvoiceid = int.Parse(repost[1]);
                }
            }


            if (repostinvoiceid == 0)
            {
                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Path = Properties.Settings.Default.ListenDirectory;
                watcher.EnableRaisingEvents = true;
                watcher.NotifyFilter = NotifyFilters.FileName;
                watcher.Filter = "*.csv";
                watcher.Created += new FileSystemEventHandler(OnChanged);

                WriteMessage("Watcher Started for path " + watcher.Path + " at " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString());
                //Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Gray;

                AppStartTime = DateTime.Now;

                //Check top see if any files exist in the upload directory that need to be processed
                // Get the files
                DirectoryInfo info = new DirectoryInfo(Properties.Settings.Default.ListenDirectory);
                FileInfo[] files = info.GetFiles();

                foreach (FileInfo file in files)
                {
                    //Process the file if it was added before the appp started
                    if (file.LastAccessTime < AppStartTime)
                    {
                        FileSystemEventArgs e = new FileSystemEventArgs(WatcherChangeTypes.Created, Properties.Settings.Default.ListenDirectory, file.Name);
                        OnChanged(file, e);
                    }
                }


                // wait not to end
                new System.Threading.AutoResetEvent(false).WaitOne();
            }
            else    //Repost the selected exceptions
            {
                WriteMessage("Re-Posting exceptions for invoice: " + repostinvoiceid.ToString());
                // new System.Threading.AutoResetEvent(false).WaitOne();

                string constring = ConfigurationManager.ConnectionStrings["BILSIBAP"].ConnectionString;
                SqlConnection conn = new SqlConnection(constring);

                OutputAramexCSV(conn, repostinvoiceid, true);

                //Set the status 
                UpdateInvoiceStatus(conn, repostinvoiceid, 17); //Exceptions reposted

                WriteMessage("Exceptions reposted");
            }
        }


        static void OnChanged(object sender, FileSystemEventArgs e)
        {

            string constring = ConfigurationManager.ConnectionStrings["BILSIBAP"].ConnectionString;
            int invoiceid = 0;

            SqlConnection conn = new SqlConnection(constring);

            try
            {
                conn.Open();
            }
            catch (Exception exc)
            {
                PostError(exc.Message);
                return;
            }

            //Create a new imported invoice record
            try
            {
                SqlCommand cmdInvoice = new SqlCommand();
                cmdInvoice.CommandType = CommandType.StoredProcedure;
                cmdInvoice.CommandText = "dbo.uspInsertImportedInvoice";
                cmdInvoice.Parameters.AddWithValue("@FileName", e.Name);
                cmdInvoice.Parameters.AddWithValue("@Type", 0);     //Unknown
                cmdInvoice.Connection = conn;
                invoiceid = Convert.ToInt32(cmdInvoice.ExecuteScalar());
                cmdInvoice.Dispose();

                WriteMessage("Created Invoice Number: " + invoiceid.ToString());
            }
            catch (Exception exc)
            {
                PostError(exc.Message);
                return;
            }

            UpdateInvoiceStatus(conn, invoiceid, 1);  //Loading File
            WriteMessage("Reading file: " + e.Name);

            CompMessage.InvoiceID = invoiceid;
            DataTable dt = new DataTable();

            //Use this function to wait for a large file to copy so it is complete before loading
            if (GetIdleFile(e.FullPath))
            {
                dt = ReadCsv(e.FullPath);

                if (dt.Rows.Count == 0)
                {
                    PostError("File does not contain any row data");
                    return;
                }
            }

            UpdateInvoiceStatus(conn, invoiceid, 2);  //File Loaded
            WriteMessage("File Read: " + dt.Rows.Count.ToString() + " rows, " + dt.Columns.Count.ToString() + " columns");

            int invtotal = dt.Rows.Count;
            CompMessage.RecordsLoadedCount = invtotal;

            //Figure out the file format based on the first field 
            string firstfield = dt.Columns[0].ColumnName;

            switch (firstfield)
            {
                case "ID":              //Aramex  TypeID = 1
                    UpdateInvoiceType(conn, invoiceid, 1);

                    //Double Check a number of columns to see if this is the correct file format
                    if (dt.Columns.Contains("Entity") == false)
                    {
                        PostError("File is not the correct format. Data does not contain column 'Entity'");
                        return;
                    }

                    if (dt.Columns.Contains("AccountID") == false)
                    {
                        PostError("File error. Data does not contain column 'AccountID'");
                        return;
                    }

                    //Get the invoice number
                    InvoiceNumber = dt.Rows[0]["DocumentID"].ToString();
                    CompMessage.InvoiceNumber = InvoiceNumber;
                    WriteMessage("File contains invoice: " + InvoiceNumber);

                    try
                    {
                        UpdateInvoiceNumber(conn, invoiceid, InvoiceNumber);
                    }
                    catch (Exception exc)       //Error will be raised if duplicate invoice number is found, will prevent invoices from loading twice
                    {
                        File.Delete(e.FullPath);
                        PostError(exc.Message);
                        return;
                    }

                    UpdateInvoiceStatus(conn, invoiceid, 4);  //Loading Details
                    WriteMessage("Loading Aramex file...");

                    LoadAramex(invoiceid, e.Name, dt, conn);

                    UpdateInvoiceStatus(conn, invoiceid, 5);  //Details Loaded
                    WriteMessage("Aramex File Loaded");

                    if (invoiceid > 0)
                    {
                        UpdateInvoiceStatus(conn, invoiceid, 6);    //Getting Rates
                        WriteMessage("Getting Aramex Rates..");
                        GetAramexCharges(conn, invoiceid, invtotal);
                        WriteMessage("Aramex Rates Loaded");
                        UpdateInvoiceStatus(conn, invoiceid, 7);    //Rates Loaded

                        //Process the details for date based exceptions
                        WriteMessage("Processing detail records..");
                        UpdateInvoiceStatus(conn, invoiceid, 12);    //Processing data

                        ProcessAramexDetails(conn, invoiceid);
                        UpdateInvoiceStatus(conn, invoiceid, 13);    //Processing data
                        WriteMessage("Detail records processed");

                        WriteMessage("Generating UFOS output files..");
                        OutputAramexCSV(conn, invoiceid, false);

                        CompMessage.ExceptionCount = ExceptionCount;
                        UpdateInvoiceStatus(conn, invoiceid, 14); //UFOS Files saved
                        SendCompletionMessage(CompMessage);

                        //Move the file to the loaded directory
                        string filename = Path.GetFileName(e.Name);
                        try
                        {
                            if (File.Exists(Path.Combine(Properties.Settings.Default.MoveDirectory, filename)))
                            {
                                File.Delete(Path.Combine(Properties.Settings.Default.MoveDirectory, filename));
                                PostError("Source file has been overwritten in loaded directory");
                            }

                            File.Move(Path.Combine(Properties.Settings.Default.ListenDirectory, filename), Path.Combine(Properties.Settings.Default.MoveDirectory, filename));
                        }
                        catch (Exception exc)
                        {
                            PostError(exc.Message);
                        }
                    }

                    break;

                case "Line_Number":     //Canada Post   TypeID = 4


                    //Double Check a number of columns to see if this is the correct file format
                    if (dt.Columns.Contains("Payer_Customer_Number") == false)
                    {
                        PostError("File is not the correct format. Data does not contain column 'Payer_Customer_Number'");
                        return;
                    }

                    UpdateInvoiceType(conn, invoiceid, 4);      // Canada Post

                    UpdateInvoiceStatus(conn, invoiceid, 4);    //Loading Details
                    WriteMessage("Loading Canada Post file...");

                    LoadCanadaPost(invoiceid, e.Name, dt, conn);

                    UpdateInvoiceStatus(conn, invoiceid, 5);  //Details Loaded
                    WriteMessage("Canada Post File Loaded");

                    if (invoiceid > 0)
                    {
                        UpdateInvoiceStatus(conn, invoiceid, 6);    //Getting Rates
                        WriteMessage("Getting Canada Post Rates..");

                        GetCanadaPostCharges(conn, invoiceid, invtotal);

                        WriteMessage("Canada Post Rates Loaded");
                        UpdateInvoiceStatus(conn, invoiceid, 7);    //Rates Loaded

                        //Process the details for date based exceptions
                        WriteMessage("Processing detail records..");
                        UpdateInvoiceStatus(conn, invoiceid, 12);    //Processing data

                        ProcessAramexDetails(conn, invoiceid);
                        UpdateInvoiceStatus(conn, invoiceid, 13);    //Processing data
                        WriteMessage("Detail records processed");

                        WriteMessage("Generating UFOS output files..");
                        OutputAramexCSV(conn, invoiceid, false);

                        UpdateInvoiceStatus(conn, invoiceid, 14); //UFOS Files saved
                    }

                    break;

                case "Master_EDI_No":   //Fed Ed   TypeID = 2

                    WriteMessage("Processing FedEx file..");

                    invoiceid = LoadFedEx(e.Name, dt, conn);

                    /*
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        Console.WriteLine(dt.Columns[i].ColumnName + " varchar(20) null,");
                        // WriteMessage("Found column: '" + dt.Columns[i].ColumnName + "'");
                    }
                    */

                    if (invoiceid > 0)
                    {
                        WriteMessage("Getting FedEx Rates..");
                        GetFedExCharges(conn, invoiceid);
                    }

                    break;

                case "REC_NUM":         //Purolator   TypeID = 3

                    WriteMessage("Processing Purolator file...");

                    invoiceid = LoadPurolator(e.Name, dt, conn);

                    if (invoiceid > 0)
                    {
                        WriteMessage("Getting FedEx Rates..");
                        GetPurolatorCharges(conn, invoiceid);
                    }


                    /*
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        //Console.WriteLine(dt.Columns[i].ColumnName + " varchar(20) null,");
                        WriteMessage("Found column: '" + dt.Columns[i].ColumnName + "'");
                    }
                    */

                    break;



                default:

                    PostError("Unknown file type. First field: " + firstfield);
                    break;
            }

            Console.CursorLeft = 0;
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            WriteMessage("File Processed: " + e.Name + " " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString());

        }

        static void LoadCanadaPost(int InvoiceID, string FileName, DataTable dt, SqlConnection conn)
        {
            int invcnt = 0;
            int invtotal = dt.Rows.Count;
            foreach (DataRow dr in dt.Rows)
            {

                try
                {
                    drawTextProgressBar("Processing Line", invcnt, invtotal);
                    invcnt++;

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.uspCanadaPostInsertInvoiceDetail";
                    cmd.Connection = conn;

                    cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
                    cmd.Parameters.AddWithValue("@Line_Number", dr["Line_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Record_Type", dr["Record_Type_(Summary=S_or_Detail=D)"].ToString());
                    cmd.Parameters.AddWithValue("@Interface_Time", dr["Interface_System_Date_&_Time"].ToString());
                    cmd.Parameters.AddWithValue("@Payer_Customer_Number", dr["Payer_Customer_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Payer_Customer_Name", dr["Payer_Customer_Name"].ToString());
                    cmd.Parameters.AddWithValue("@Payer_Customer_Address", dr["Payer_Customer_Address_Street_2"].ToString());
                    cmd.Parameters.AddWithValue("@Payer_Customer_City", dr["Payer_Customer_City"].ToString());
                    cmd.Parameters.AddWithValue("@Payer_Customer_Prov", dr["Payer_Customer_Province"].ToString());
                    cmd.Parameters.AddWithValue("@Payer_Customer_Postal_Code", dr["Payer_Customer_Postal_Code"].ToString());
                    cmd.Parameters.AddWithValue("@Document_Type", dr["Document_Type"].ToString());
                    cmd.Parameters.AddWithValue("@Invoice_Number", dr["Invoice_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Manifest_Number", dr["Manifest,_SOM,_or_PO_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Manifest_Date", dr["Manifest,_SOM,_or_PO_Date"].ToString());
                    cmd.Parameters.AddWithValue("@Customer_Order_Reference", dr["Customer_Order_Reference"].ToString());
                    cmd.Parameters.AddWithValue("@Order_Number_Returns", dr["Order_Number/Original_PIN_ID_for_Returns"].ToString());
                    cmd.Parameters.AddWithValue("@Delivery_Number_Returns", dr["Delivery_Number/Original_Manifest_for_Returns"].ToString());
                    cmd.Parameters.AddWithValue("@Sold_To_Customer_Number", dr["Sold_to_Customer_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Sold_To_Customer_Name", dr["Sold_to_Customer_Name"].ToString());
                    cmd.Parameters.AddWithValue("@Sold_To_Customer_Address", dr["Sold_to_Customer_Address_Street_2"].ToString());
                    cmd.Parameters.AddWithValue("@Sold_To_Customer_City", dr["Sold_to_Customer_City"].ToString());
                    cmd.Parameters.AddWithValue("@Sold_To_Customer_Province", dr["Sold_to_Customer_Province"].ToString());
                    cmd.Parameters.AddWithValue("@Sold_To_Customer_PostalCode", dr["Sold_to_Customer_Postal_Code"].ToString());
                    cmd.Parameters.AddWithValue("@Agreement_Number", dr["Agreement_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Invoice_Date", dr["Invoice_Date"].ToString());
                    cmd.Parameters.AddWithValue("@Invoice_Due_Date", dr["Invoice_Due_Date"].ToString());
                    cmd.Parameters.AddWithValue("@Article_Number", dr["Article_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Service_Description", dr["Service_Description"].ToString());
                    cmd.Parameters.AddWithValue("@Product_Identifier", dr["Product_Identifier"].ToString());
                    cmd.Parameters.AddWithValue("@Additional_Info1", dr["Additional_Info_#1"].ToString());
                    cmd.Parameters.AddWithValue("@Additional_Info2", dr["Additional_Info_#2"].ToString());
                    cmd.Parameters.AddWithValue("@Quantity_Shipped", dr["Quantity_Shipped"].ToString());
                    cmd.Parameters.AddWithValue("@Weight_Per_Piece", dr["Weight_per_piece"].ToString());
                    cmd.Parameters.AddWithValue("@Rate_Code", dr["Rate_Code/Return_Service_Description"].ToString());
                    cmd.Parameters.AddWithValue("@Base_Charge", dr["Base_Charge"].ToString());
                    cmd.Parameters.AddWithValue("@Qty_For_Additional_Coverage", dr["Quantity_for_Additional_Coverage"].ToString());
                    cmd.Parameters.AddWithValue("@Addtional_Coverage_Charge", dr["Additional_Coverage_Charge"].ToString());
                    cmd.Parameters.AddWithValue("@Qty_For_Signature_Option", dr["Quantity_for_Signature_Option"].ToString());
                    cmd.Parameters.AddWithValue("@Signature_Option_Charge", dr["Signature_Option_Charge"].ToString());
                    cmd.Parameters.AddWithValue("@Fuel_Surcharge_Rate", dr["Fuel_Surcharge_Rate_(%)"].ToString());
                    cmd.Parameters.AddWithValue("@Fuel_Surcharge_Amount", dr["Fuel_Surcharge_Amount"].ToString());
                    cmd.Parameters.AddWithValue("@Net_Charge", dr["Net_Charge"].ToString());
                    cmd.Parameters.AddWithValue("@GST_Amount", dr["GST_Amount"].ToString());
                    cmd.Parameters.AddWithValue("@HST_Amount", dr["HST_Amount"].ToString());
                    cmd.Parameters.AddWithValue("@PST_Amount", dr["PST_Amount"].ToString());
                    cmd.Parameters.AddWithValue("@GST_PST_Tax_Status", dr["GST_/_HST_Tax_Status"].ToString());
                    cmd.Parameters.AddWithValue("@PST_Tax_Status", dr["PST_Tax_Status"].ToString());
                    cmd.Parameters.AddWithValue("@Total_Charges", dr["Total_Charges"].ToString());
                    cmd.Parameters.AddWithValue("@Provincial_Tax_Code", dr["Provincial_Tax_Code"].ToString());
                    cmd.Parameters.AddWithValue("@Declared_Product_Num", dr["Declared_Product_Num"].ToString());
                    cmd.Parameters.AddWithValue("@Declared_Weight", dr["DECLARED_WGT"].ToString());
                    cmd.Parameters.AddWithValue("@Customer_Ref1", dr["CUSTOMER_REF1"].ToString());
                    cmd.Parameters.AddWithValue("@Customer_Ref2", dr["CUSTOMER_REF2"].ToString());
                    cmd.Parameters.AddWithValue("@Customer_Ref3", dr["CUSTOMER_REF3"].ToString());
                    cmd.Parameters.AddWithValue("@Customer_Ref4", dr["CUSTOMER_REF4"].ToString());
                    cmd.Parameters.AddWithValue("@Customer_Ref5", dr["CUSTOMER_REF5"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_From_Company", dr["SHIP_FROM_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_From_Address", dr["SHIP_FROM_ADDR"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_From_City", dr["SHIP_FROM_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_From_Prov", dr["SHIP_FROM_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_From_Postal_Code", dr["SHIP_FROM_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_From_Country", dr["SHIP_FROM_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_Attn", dr["SHIP_TO_ATTN"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_Company", dr["SHIP_TO_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_Address", dr["SHIP_TO_ADDR"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_City", dr["SHIP_TO_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_Prov", dr["SHIP_TO_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_Postal_Code", dr["SHIP_TO_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_To_Country", dr["SHIP_TO_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@Service_Date", dr["SERVICE_DATE"].ToString());

                    cmd.ExecuteNonQuery();

                }
                catch (Exception exc)
                {
                    PostError("Row " + invcnt.ToString() + ":" + exc.Message);
                    conn.Close();
                    InsertException(conn, InvoiceID, 1, invcnt, exc.Message);
                }
            }
        }

        static int LoadPurolator(string FileName, DataTable dt, SqlConnection conn)
        {
            int invcnt = 0;
            int invtotal = dt.Rows.Count;
            string invid = "0";

            try
            {
                SqlCommand cmdInvoice = new SqlCommand();
                cmdInvoice.CommandType = CommandType.StoredProcedure;
                cmdInvoice.CommandText = "dbo.uspInsertImportedInvoice";
                cmdInvoice.Parameters.AddWithValue("@FileName", FileName);
                cmdInvoice.Parameters.AddWithValue("@Type", 3);     //Purolator
                cmdInvoice.Connection = conn;
                invid = cmdInvoice.ExecuteScalar().ToString();
                cmdInvoice.Dispose();
            }
            catch (Exception exc)
            {
                PostError(exc.Message);
                return 0;
            }


            foreach (DataRow dr in dt.Rows)
            {

                try
                {
                    drawTextProgressBar("Processing Line", invcnt, invtotal);
                    invcnt++;
                    //System.Threading.Thread.Sleep(10);

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.uspPurolatorInsertInvoiceDetail";
                    cmd.Connection = conn;

                    cmd.Parameters.AddWithValue("@InvoiceID", invid);

                    cmd.Parameters.AddWithValue("@REC_NUM", dr["REC_NUM"].ToString());
                    cmd.Parameters.AddWithValue("@REC_TYPE", dr["REC_TYPE"].ToString());
                    cmd.Parameters.AddWithValue("@INV_NUM", dr["INV_NUM"].ToString());

                    cmd.Parameters.AddWithValue("@INV_DATE", dr["INV_DATE"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_ACC", dr["PAYER_ACC"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_COMPANY", dr["PAYER_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_ATTN", dr["PAYER_ATTN"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_ADDR", dr["PAYER_ADDR"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_CITY", dr["PAYER_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_PROV", dr["PAYER_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_POSTAL_CODE", dr["PAYER_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@PAYER_COUNTRY", dr["PAYER_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@INV_NET_AMT", dr["INV_NET_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@INV_GST", dr["INV_GST"].ToString());
                    cmd.Parameters.AddWithValue("@INV_PST", dr["INV_PST"].ToString());
                    cmd.Parameters.AddWithValue("@INV_QST", dr["INV_QST"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST", dr["INV_HST"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST_BC", dr["INV_HST_BC"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST_NB", dr["INV_HST_NB"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST_NL", dr["INV_HST_NL"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST_NS", dr["INV_HST_NS"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST_ON", dr["INV_HST_ON"].ToString());
                    cmd.Parameters.AddWithValue("@INV_HST_PEI", dr["INV_HST_PEI"].ToString());
                    cmd.Parameters.AddWithValue("@INV_TOTAL_AMT", dr["INV_TOTAL_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@CURRENCY", dr["CURRENCY"].ToString());
                    cmd.Parameters.AddWithValue("@INV_DUE_DATE", dr["INV_DUE_DATE"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_DETAIL_REC", dr["NUM_OF_DETAIL_REC"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_SHIPMENTS", dr["NUM_OF_SHIPMENTS"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_PCS", dr["NUM_OF_PCS"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_PREPAID_SHIPMENTS", dr["NUM_OF_PREPAID_SHIPMENTS"].ToString());
                    cmd.Parameters.AddWithValue("@PREPAID_AMT", dr["PREPAID_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_COLLECT_SHIPMENTS", dr["NUM_OF_COLLECT_SHIPMENTS"].ToString());
                    cmd.Parameters.AddWithValue("@COLLECT_AMT", dr["COLLECT_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_3RDPARTY_SHIPMENTS", dr["NUM_OF_3RDPARTY_SHIPMENTS"].ToString());
                    cmd.Parameters.AddWithValue("@3RDPARTY_AMT", dr["3RDPARTY_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@NUM_OF_ACCESSORIAL", dr["NUM_OF_ACCESSORIAL"].ToString());
                    cmd.Parameters.AddWithValue("@ACCESSORIAL_AMT", dr["ACCESSORIAL_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@FUEL_AMT", dr["FUEL_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_COMPANY", dr["MAIL_TO_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_ATTN", dr["MAIL_TO_ATTN"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_ADDR1", dr["MAIL_TO_ADDR1"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_ADDR2", dr["MAIL_TO_ADDR2"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_CITY", dr["MAIL_TO_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_PROV", dr["MAIL_TO_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_POSTAL_CODE", dr["MAIL_TO_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@MAIL_TO_COUNTRY", dr["MAIL_TO_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_COMPANY", dr["REMIT_TO_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_ADDR1", dr["REMIT_TO_ADDR1"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_ADDR2", dr["REMIT_TO_ADDR2"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_CITY", dr["REMIT_TO_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_PROV", dr["REMIT_TO_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_POSTAL_CODE", dr["REMIT_TO_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@REMIT_TO_COUNTRY", dr["REMIT_TO_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_ACC", dr["SOLD_TO_ACC"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_COMPANY", dr["SOLD_TO_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_ATTN", dr["SOLD_TO_ATTN"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_ADDR", dr["SOLD_TO_ADDR"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_CITY", dr["SOLD_TO_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_PROV", dr["SOLD_TO_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_POSTAL_CODE", dr["SOLD_TO_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_COUNTRY", dr["SOLD_TO_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_NET_AMT", dr["SOLD_TO_NET_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_GST", dr["SOLD_TO_GST"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_PST", dr["SOLD_TO_PST"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_QST", dr["SOLD_TO_QST"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST", dr["SOLD_TO_HST"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST_BC", dr["SOLD_TO_HST_BC"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST_NB", dr["SOLD_TO_HST_NB"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST_NL", dr["SOLD_TO_HST_NL"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST_NS", dr["SOLD_TO_HST_NS"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST_ON", dr["SOLD_TO_HST_ON"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_HST_PEI", dr["SOLD_TO_HST_PEI"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_TOTAL_AMT", dr["SOLD_TO_TOTAL_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_NUM_OF_SHIPMENTS ", dr["SOLD_TO_NUM_OF_SHIPMENTS"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_NUM_OF_PCS ", dr["SOLD_TO_NUM_OF_PCS"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_SHIPMENT_NET_AMT", dr["SOLD_TO_SHIPMENT_NET_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_AC_NET_AMT", dr["SOLD_TO_AC_NET_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_FUEL_AMT", dr["SOLD_TO_FUEL_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@SOLD_TO_CONTRACT", dr["SOLD_TO_CONTRACT"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPPING_METHOD", dr["SHIPPING_METHOD"].ToString());
                    cmd.Parameters.AddWithValue("@SERVICE_DATE", dr["SERVICE_DATE"].ToString());
                    cmd.Parameters.AddWithValue("@BOL_MNFST_NUM", dr["BOL_MNFST_NUM"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_PIN", dr["SHIPMENT_PIN"].ToString());
                    cmd.Parameters.AddWithValue("@LINE_ITEM ", dr["LINE_ITEM"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPPING_METHOD_DESC", dr["SHIPPING_METHOD_DESC"].ToString());
                    cmd.Parameters.AddWithValue("@BOL_MNFST_FLAG", dr["BOL_MNFST_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_PRODUCT_NUM", dr["ACTUAL_PRODUCT_NUM"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_PRODUCT_DESC", dr["ACTUAL_PRODUCT_DESC"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_WGT", dr["ACTUAL_WGT"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_WGT_UOM", dr["ACTUAL_WGT_UOM"].ToString());
                    cmd.Parameters.AddWithValue("@METHOD_OF_PAYMENT", dr["METHOD_OF_PAYMENT"].ToString());
                    cmd.Parameters.AddWithValue("@PIECES", dr["PIECES"].ToString());
                    cmd.Parameters.AddWithValue("@DECLARED_PRODUCT_NUM", dr["DECLARED_PRODUCT_NUM"].ToString());
                    cmd.Parameters.AddWithValue("@DECLARED_PRODUCT_DESC", dr["DECLARED_PRODUCT_DESC"].ToString());
                    cmd.Parameters.AddWithValue("@DECLARED_WGT", dr["DECLARED_WGT"].ToString());
                    cmd.Parameters.AddWithValue("@DECLARED_WGT_UOM", dr["DECLARED_WGT_UOM"].ToString());
                    cmd.Parameters.AddWithValue("@RETURN_FLAG", dr["RETURN_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@CUSTOMER_REF1", dr["CUSTOMER_REF1"].ToString());
                    cmd.Parameters.AddWithValue("@CUSTOMER_REF2", dr["CUSTOMER_REF2"].ToString());
                    cmd.Parameters.AddWithValue("@CUSTOMER_REF3", dr["CUSTOMER_REF3"].ToString());
                    cmd.Parameters.AddWithValue("@CUSTOMER_REF4", dr["CUSTOMER_REF4"].ToString());
                    cmd.Parameters.AddWithValue("@CUSTOMER_REF5", dr["CUSTOMER_REF5"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_COMPANY", dr["SHIP_FROM_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_ATTN", dr["SHIP_FROM_ATTN"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_ADDR", dr["SHIP_FROM_ADDR"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_CITY", dr["SHIP_FROM_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_PROV", dr["SHIP_FROM_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_POSTAL_CODE", dr["SHIP_FROM_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_FROM_COUNTRY", dr["SHIP_FROM_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_COMPANY", dr["SHIP_TO_COMPANY"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_ATTN", dr["SHIP_TO_ATTN"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_ADDR", dr["SHIP_TO_ADDR"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_CITY", dr["SHIP_TO_CITY"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_PROV", dr["SHIP_TO_PROV"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_POSTAL_CODE", dr["SHIP_TO_POSTAL_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@SHIP_TO_COUNTRY", dr["SHIP_TO_COUNTRY"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_BASE_AMT", dr["SHIPMENT_BASE_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_GST", dr["SHIPMENT_GST"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_PST", dr["SHIPMENT_PST"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_QST", dr["SHIPMENT_QST"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_HST", dr["SHIPMENT_HST"].ToString());
                    cmd.Parameters.AddWithValue("@SHIPMENT_TOTAL_AMT", dr["SHIPMENT_TOTAL_AMT"].ToString());
                    cmd.Parameters.AddWithValue("@CUBING_FACTOR_1", dr["CUBING_FACTOR_1"].ToString());
                    cmd.Parameters.AddWithValue("@FUEL_SURCHARGE", dr["FUEL_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@DNGR_GOODS", dr["DNGR_GOODS"].ToString());
                    cmd.Parameters.AddWithValue("@DNGR_GOODS_ACTUAL_FLAG", dr["DNGR_GOODS_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@DNGR_GOODS_DECLARED_FLAG", dr["DNGR_GOODS_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@COS_SURCHARGE", dr["COS_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@COS_ACTUAL_FLAG", dr["COS_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@COS_DECLARED_FLAG", dr["COS_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@EXPRESS_CHEQUE", dr["EXPRESS_CHEQUE"].ToString());
                    cmd.Parameters.AddWithValue("@EC_ACTUAL_FLAG", dr["EC_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@EC_DECLARED_FLAG", dr["EC_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@SATURDAY_DELIVERY", dr["SATURDAY_DELIVERY"].ToString());
                    cmd.Parameters.AddWithValue("@SAT_DELIV_ACTUAL_FLAG", dr["SAT_DELIV_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@SAT_DELIV_DECLARED_FLAG", dr["SAT_DELIV_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@SATURDAY_PICKUP", dr["SATURDAY_PICKUP"].ToString());
                    cmd.Parameters.AddWithValue("@SAT_PICKUP_ACTUAL_FLAG", dr["SAT_PICKUP_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@SAT_PICKUP_DECLARED_FLAG", dr["SAT_PICKUP_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@SPECIAL_HANDLING", dr["SPECIAL_HANDLING"].ToString());
                    cmd.Parameters.AddWithValue("@SH_ACTUAL_FLAG", dr["SH_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@SH_DECLARED_FLAG", dr["SH_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@COLLECT_3RDPARTY_SURCHARGE", dr["COLLECT_3RDPARTY_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@COLLECT_3RDPARTY_ACTUAL_FLAG", dr["COLLECT_3RDPARTY_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@COLLECT_3RDPARTY_DECLARED_FLAG", dr["COLLECT_3RDPARTY_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@DECLARED_VALUE_SURCHARGE", dr["DECLARED_VALUE_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@DV_ACTUAL_FLAG", dr["DV_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@DV_DECLARED_FLAG", dr["DV_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@MULTI_PIECE_SURCHARGE", dr["MULTI_PIECE_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@RESIDENTIAL_AREA_SURCHARGE", dr["RESIDENTIAL_AREA_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@BEYOND_ORIGIN_SURCHARGE", dr["BEYOND_ORIGIN_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@BEYOND_DEST_SURCHARGE", dr["BEYOND_DEST_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@MISSING_ACCOUNT_SURCHARGE", dr["MISSING_ACCOUNT_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@AIR_CARGO_NAVCAN_SURCHARGE", dr["AIR_CARGO_NAVCAN_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@AIR_CARGO_FUEL_SURCHARGE", dr["AIR_CARGO_FUEL_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@AIR_CARGO_DNGR_GD_SURCHARGE", dr["AIR_CARGO_DNGR_GD_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@AIR_CARGO_DNGR_GD_ACTUAL_FLAG", dr["AIR_CARGO_DNGR_GD_ACTUAL_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@AIR_CARGO_DNGR_GD_DECLARED_FLAG", dr["AIR_CARGO_DNGR_GD_DECLARED_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@PIECE_PIN", dr["PIECE_PIN"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_ACTUAL_WGT", dr["PCS_ACTUAL_WGT"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_ACTUAL_WGT_UOM", dr["PCS_ACTUAL_WGT_UOM"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_WGT_FLAG", dr["ACTUAL_WGT_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_HEIGHT", dr["ACTUAL_HEIGHT"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_WIDTH", dr["ACTUAL_WIDTH"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_LENGTH", dr["ACTUAL_LENGTH"].ToString());
                    cmd.Parameters.AddWithValue("@ACTUAL_DIMENSION_UOM", dr["ACTUAL_DIMENSION_UOM"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_DECLARED_WGT", dr["PCS_DECLARED_WGT"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_DECLARED_WGT_UOM", dr["PCS_DECLARED_WGT_UOM"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_DECLARED_HEIGHT", dr["PCS_DECLARED_HEIGHT"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_DECLARED_WIDTH", dr["PCS_DECLARED_WIDTH"].ToString());
                    cmd.Parameters.AddWithValue("@PCS_DECLARED_LENGTH", dr["PCS_DECLARED_LENGTH"].ToString());
                    cmd.Parameters.AddWithValue("@DECLARED_DIMENSION_UOM", dr["DECLARED_DIMENSION_UOM"].ToString());
                    cmd.Parameters.AddWithValue("@SPECIAL_HANDLING_FLAG", dr["SPECIAL_HANDLING_FLAG"].ToString());
                    cmd.Parameters.AddWithValue("@RESIDENTIAL_PICKUP_SURCHARGE", dr["RESIDENTIAL_PICKUP_SURCHARGE"].ToString());
                    cmd.Parameters.AddWithValue("@RESIDENTIAL_SIGNATURE", dr["RESIDENTIAL_SIGNATURE"].ToString());
                    cmd.Parameters.AddWithValue("@MANUAL_SHPT_FEE", dr["MANUAL_SHPT_FEE"].ToString());
                    cmd.Parameters.AddWithValue("@ORIGIN_ZONE_ID", dr["ORIGIN_ZONE_ID"].ToString());
                    cmd.Parameters.AddWithValue("@RATE_CODE", dr["RATE_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@ORIGIN_BEYOND_RATE_CODE", dr["ORIGIN_BEYOND_RATE_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@DEST_BEYOND_RATE_CODE", dr["DEST_BEYOND_RATE_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@GST_HST_TAX_NUM", dr["GST_HST_TAX_NUM"].ToString());
                    cmd.Parameters.AddWithValue("@QST_TAX_NUM", dr["QST_TAX_NUM"].ToString());

                    cmd.ExecuteNonQuery();

                }
                catch (Exception exc)
                {
                    PostError("Row " + invcnt.ToString() + ":" + exc.Message);
                    conn.Close();
                    return 0;
                }
            }

            return int.Parse(invid);
        }


        static int LoadFedEx(string FileName, DataTable dt, SqlConnection conn)
        {
            string invid;
            int invcnt = 0;
            int invtotal = dt.Rows.Count;

            try
            {
                SqlCommand cmdInvoice = new SqlCommand();
                cmdInvoice.CommandType = CommandType.StoredProcedure;
                cmdInvoice.CommandText = "dbo.uspInsertImportedInvoice";
                cmdInvoice.Parameters.AddWithValue("@FileName", FileName);
                cmdInvoice.Parameters.AddWithValue("@Type", 2);     //FedEx
                cmdInvoice.Connection = conn;
                invid = cmdInvoice.ExecuteScalar().ToString();
                cmdInvoice.Dispose();
            }
            catch (Exception exc)
            {
                PostError(exc.Message);
                return 0;
            }


            foreach (DataRow dr in dt.Rows)
            {

                try
                {
                    drawTextProgressBar("Processing Line", invcnt, invtotal);
                    invcnt++;

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.uspFedExInsertInvoiceDetail";
                    cmd.Connection = conn;

                    cmd.Parameters.AddWithValue("@InvoiceID", invid);
                    cmd.Parameters.AddWithValue("@Master_EDI_No", dr["Master_EDI_No"].ToString());
                    cmd.Parameters.AddWithValue("@Invoice_Number", dr["Invoice_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Invoice_Date", dr["Invoice_Date"].ToString());
                    cmd.Parameters.AddWithValue("@Ground_Tracking_Number", dr["Ground_Tracking_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Tracking_Number", dr["Tracking_Number"].ToString());
                    cmd.Parameters.AddWithValue("@Ship_Date", dr["Ship_Date"].ToString());
                    cmd.Parameters.AddWithValue("@Pkg", dr["Pkg"].ToString());
                    cmd.Parameters.AddWithValue("@Ref_1", dr["Ref_1"].ToString());
                    cmd.Parameters.AddWithValue("@Cust_PO_No", dr["Cust_PO_No"].ToString());
                    cmd.Parameters.AddWithValue("@Net_Chrg", dr["Net_Chrg"].ToString());
                    cmd.Parameters.AddWithValue("@Curr", dr["Curr"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_1", dr["Chrg_1"].ToString());
                    cmd.Parameters.AddWithValue("@Freight_Amt", dr["Freight_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_2", dr["Chrg_2"].ToString());
                    cmd.Parameters.AddWithValue("@Vol_Disc", dr["Vol_Disc"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_3", dr["Chrg_3"].ToString());
                    cmd.Parameters.AddWithValue("@Earned_Disc", dr["Earned_Disc"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_4", dr["Chrg_4"].ToString());
                    cmd.Parameters.AddWithValue("@Auto_Disc", dr["Auto_Disc"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_5", dr["Chrg_5"].ToString());
                    cmd.Parameters.AddWithValue("@Perf_Price_Disc", dr["Perf_Price_Disc"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_6", dr["Chrg_6"].ToString());
                    cmd.Parameters.AddWithValue("@Fuel_Amt", dr["Fuel_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_7", dr["Chrg_7"].ToString());
                    cmd.Parameters.AddWithValue("@Resi_Amt", dr["Resi_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_8", dr["Chrg_8"].ToString());
                    cmd.Parameters.AddWithValue("@DAS_Amt", dr["DAS_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_9", dr["Chrg_9"].ToString());
                    cmd.Parameters.AddWithValue("@On_Call_Amt", dr["On_Call_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_10", dr["Chrg_10"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_11", dr["Chrg_11"].ToString());
                    cmd.Parameters.AddWithValue("@Sign_Svc_Amt", dr["Sign_Svc_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_12", dr["Chrg_12"].ToString());
                    cmd.Parameters.AddWithValue("@Sat_Amt", dr["Sat_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_13", dr["Chrg_13"].ToString());
                    cmd.Parameters.AddWithValue("@Addn_Hndlg_Amt", dr["Addn_Hndlg_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_14", dr["Chrg_14"].ToString());
                    cmd.Parameters.AddWithValue("@Adr_Corr_Amt", dr["Adr_Corr_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_15", dr["Chrg_15"].ToString());
                    cmd.Parameters.AddWithValue("@GST_Amt", dr["GST_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_16", dr["Chrg_16"].ToString());
                    cmd.Parameters.AddWithValue("@HST_Amt", dr["HST_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_17", dr["Chrg_17"].ToString());
                    cmd.Parameters.AddWithValue("@PST_Amt", dr["PST_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_18", dr["Chrg_18"].ToString());
                    cmd.Parameters.AddWithValue("@Duty_Amt", dr["Duty_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_19", dr["Chrg_19"].ToString());
                    cmd.Parameters.AddWithValue("@Adv_Fee_Amt", dr["Adv_Fee_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_20", dr["Chrg_20"].ToString());
                    cmd.Parameters.AddWithValue("@Orig_VAT_Amt", dr["Orig_VAT_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_21", dr["Chrg_21"].ToString());
                    cmd.Parameters.AddWithValue("@Misc_1_Amt", dr["Misc_1_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_22", dr["Chrg_22"].ToString());
                    cmd.Parameters.AddWithValue("@Misc_2_Amt", dr["Misc_2_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Chrg_23", dr["Chrg_23"].ToString());
                    cmd.Parameters.AddWithValue("@Misc_3_Amt", dr["Misc_3_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Exchg_Rate", dr["Exchg_Rate"].ToString());
                    cmd.Parameters.AddWithValue("@Exc_Curr", dr["Exc_Curr"].ToString());
                    cmd.Parameters.AddWithValue("@Fuel_Pct", dr["Fuel_Pct"].ToString());
                    cmd.Parameters.AddWithValue("@Dec_Value", dr["Dec_Value"].ToString());
                    cmd.Parameters.AddWithValue("@Customs_Value", dr["Customs_Value"].ToString());
                    cmd.Parameters.AddWithValue("@DV_Cus_Curr", dr["DV_Cus_Curr"].ToString());
                    cmd.Parameters.AddWithValue("@Entry_Number", dr["Entry_Number"].ToString());
                    cmd.Parameters.AddWithValue("@MTWT_No", dr["MTWT_No"].ToString());
                    cmd.Parameters.AddWithValue("@Scale", dr["Scale"].ToString());
                    cmd.Parameters.AddWithValue("@Pcs", dr["Pcs"].ToString());
                    cmd.Parameters.AddWithValue("@Bill_Wt", dr["Bill_Wt"].ToString());
                    cmd.Parameters.AddWithValue("@Orig_Wt", dr["Orig_Wt"].ToString());
                    cmd.Parameters.AddWithValue("@Multi_Wt", dr["Multi_Wt"].ToString());
                    cmd.Parameters.AddWithValue("@Wt_Unit", dr["Wt_Unit"].ToString());
                    cmd.Parameters.AddWithValue("@Length", dr["Length"].ToString());
                    cmd.Parameters.AddWithValue("@Width", dr["Width"].ToString());
                    cmd.Parameters.AddWithValue("@Height", dr["Height"].ToString());
                    cmd.Parameters.AddWithValue("@Dim_Unit", dr["Dim_Unit"].ToString());
                    cmd.Parameters.AddWithValue("@Divisor", dr["Divisor"].ToString());
                    cmd.Parameters.AddWithValue("@Misc_1", dr["Misc_1"].ToString());
                    cmd.Parameters.AddWithValue("@Misc_2", dr["Misc_2"].ToString());
                    cmd.Parameters.AddWithValue("@Misc_3", dr["Misc_3"].ToString());
                    cmd.Parameters.AddWithValue("@Shipper_Name", dr["Shipper_Name"].ToString());
                    cmd.Parameters.AddWithValue("@Shipper_Company", dr["Shipper_Company"].ToString());
                    cmd.Parameters.AddWithValue("@Shipper_Dept", dr["Shipper_Dept"].ToString());
                    cmd.Parameters.AddWithValue("@Shipper_Address_1", dr["Shipper_Address_1"].ToString());
                    cmd.Parameters.AddWithValue("@Shipper_Address_2", dr["Shipper_Address_2"].ToString());
                    cmd.Parameters.AddWithValue("@Shipper_City", dr["Shipper_City"].ToString());
                    cmd.Parameters.AddWithValue("@ST", dr["ST"].ToString());
                    cmd.Parameters.AddWithValue("@Postal", dr["Postal"].ToString());
                    cmd.Parameters.AddWithValue("@Origin_Zip", dr["Origin_Zip"].ToString());
                    cmd.Parameters.AddWithValue("@Cntry1", dr["Cntry1"].ToString());
                    cmd.Parameters.AddWithValue("@Region", dr["Region"].ToString());
                    cmd.Parameters.AddWithValue("@Recipient_Name", dr["Recipient_Name"].ToString());
                    cmd.Parameters.AddWithValue("@Recipient_Company", dr["Recipient_Company"].ToString());
                    cmd.Parameters.AddWithValue("@Recipient_Address_1", dr["Recipient_Address_1"].ToString());
                    cmd.Parameters.AddWithValue("@Recipient_Address_2", dr["Recipient_Address_2"].ToString());
                    cmd.Parameters.AddWithValue("@Recipient_City", dr["Recipient_City"].ToString());
                    cmd.Parameters.AddWithValue("@ST2", dr["ST2"].ToString());
                    cmd.Parameters.AddWithValue("@Postal2", dr["Postal2"].ToString());
                    cmd.Parameters.AddWithValue("@Cntry2", dr["Cntry2"].ToString());
                    cmd.Parameters.AddWithValue("@Handling", dr["Handling"].ToString());
                    cmd.Parameters.AddWithValue("@Delivery", dr["Delivery"].ToString());
                    cmd.Parameters.AddWithValue("@Final", dr["Final"].ToString());
                    cmd.Parameters.AddWithValue("@Exceptn", dr["Exceptn"].ToString());
                    cmd.Parameters.AddWithValue("@Attempt_Date", dr["Attempt_Date"].ToString());
                    cmd.Parameters.AddWithValue("@Attempt_Time", dr["Attempt_Time"].ToString());
                    cmd.Parameters.AddWithValue("@Svc_Area", dr["Svc_Area"].ToString());
                    cmd.Parameters.AddWithValue("@COD_Amt", dr["COD_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@COD_Trkg_No", dr["COD_Trkg_No"].ToString());
                    cmd.Parameters.AddWithValue("@PDue", dr["PDue"].ToString());
                    cmd.Parameters.AddWithValue("@PDue_Inv", dr["PDue_Inv"].ToString());
                    cmd.Parameters.AddWithValue("@Svc_Pct", dr["Svc_Pct"].ToString());
                    cmd.Parameters.AddWithValue("@Rev_Threshold_Amt", dr["Rev_Threshold_Amt"].ToString());
                    cmd.Parameters.AddWithValue("@Orig_Recip_Adr_1", dr["Orig_Recip_Adr_1"].ToString());
                    cmd.Parameters.AddWithValue("@Orig_Recip_Adr_2", dr["Orig_Recip_Adr_2"].ToString());
                    cmd.Parameters.AddWithValue("@Original_City", dr["Original_City"].ToString());
                    cmd.Parameters.AddWithValue("@ST3", dr["ST3"].ToString());
                    cmd.Parameters.AddWithValue("@Postal3", dr["Postal3"].ToString());
                    cmd.Parameters.AddWithValue("@VAT_No", dr["VAT_No"].ToString());
                    cmd.Parameters.AddWithValue("@FedEx_VAT_No", dr["FedEx_VAT_No"].ToString());
                    cmd.Parameters.AddWithValue("@Cross_Ref_No", dr["Cross_Ref_No"].ToString());
                    cmd.ExecuteNonQuery();

                }
                catch (Exception exc)
                {
                    PostError("Row " + invcnt.ToString() + ":" + exc.Message);
                    conn.Close();
                    return int.Parse(invid);
                }

            }

            return int.Parse(invid);

        }

        static void LoadAramex(int InvoiceID, string FileName, DataTable dt, SqlConnection conn)
        {
            int invcnt = 0;
            int invtotal = dt.Rows.Count;

            foreach (DataRow dr in dt.Rows)
            {

                try
                {
                    invcnt++;
                    drawTextProgressBar("Processing Line", invcnt, invtotal);

                    //System.Threading.Thread.Sleep(10);
                }
                catch (Exception exc)
                {
                    conn.Close();
                    PostError(exc.Message);
                }

                try
                {
                    if (dr["ID"].ToString().Length > 0)  //Only add real values, not junk rows etc.
                    {

                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "dbo.uspAramexInsertInvoiceDetail";
                        cmd.Connection = conn;

                        cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
                        cmd.Parameters.AddWithValue("@DetailID", dr["ID"].ToString());
                        cmd.Parameters.AddWithValue("@AccountID", dr["AccountID"].ToString());
                        cmd.Parameters.AddWithValue("@TranDate", dr["TransactionDate"].ToString());
                        cmd.Parameters.AddWithValue("@HAWBID", dr["HAWBID"].ToString());
                        cmd.Parameters.AddWithValue("@AWB", dr["AWB"].ToString());
                        cmd.Parameters.AddWithValue("@DocumentID", dr["DocumentID"].ToString());
                        cmd.Parameters.AddWithValue("@Currency", dr["Currency"].ToString());
                        cmd.Parameters.AddWithValue("@Amount", dr["Amount"].ToString());
                        cmd.Parameters.AddWithValue("@OtherAmount1", dr["OtherAmount1"].ToString());
                        cmd.Parameters.AddWithValue("@OtherAmount2", dr["OtherAmount2"].ToString());
                        cmd.Parameters.AddWithValue("@OtherAmount3", dr["OtherAmount3"].ToString());
                        cmd.Parameters.AddWithValue("@OtherAmount4", dr["OtherAmount4"].ToString());
                        cmd.Parameters.AddWithValue("@OtherAmount5", dr["OtherAmount5"].ToString());
                        cmd.Parameters.AddWithValue("@ExchangeRate", dr["ExchangeRate"].ToString());
                        cmd.Parameters.AddWithValue("@CustomsAmount", dr["Customs"].ToString());
                        cmd.Parameters.AddWithValue("@CustomsCurrency", dr["CustomsCurrency"].ToString());
                        cmd.Parameters.AddWithValue("@Status", dr["Status"].ToString());
                        cmd.Parameters.AddWithValue("@CalculatedOn ", dr["CalculatedOn"].ToString());
                        cmd.Parameters.AddWithValue("@RateSheetID", dr["RateSheetID"].ToString());
                        cmd.Parameters.AddWithValue("@RateSheetDetailsID", dr["RateSheetDetailsID"].ToString());
                        cmd.Parameters.AddWithValue("@SettledBy", dr["SettledBy"].ToString());
                        cmd.Parameters.AddWithValue("@SettledDate", dr["SettledDate"].ToString());
                        cmd.Parameters.AddWithValue("@ShipperReference", dr["ShipperReference"].ToString());
                        cmd.Parameters.AddWithValue("@Origin", dr["Origin"].ToString());
                        cmd.Parameters.AddWithValue("@OriginCity", dr["OriginCity"].ToString());
                        cmd.Parameters.AddWithValue("@OriginStateProv", dr["OriginState"].ToString());
                        cmd.Parameters.AddWithValue("@OriginZipCode", dr["OriginZipCode"].ToString());
                        cmd.Parameters.AddWithValue("@OriginCountry", dr["OriginCountry"].ToString());
                        cmd.Parameters.AddWithValue("@ShipperNumber", dr["ShipperNumber"].ToString());
                        cmd.Parameters.AddWithValue("@SentBy", dr["SentBy"].ToString());
                        cmd.Parameters.AddWithValue("@ShipperName", dr["ShipperName"].ToString());
                        cmd.Parameters.AddWithValue("@ShipperAddress", dr["ShipperAddress"].ToString());
                        cmd.Parameters.AddWithValue("@ShipperTelephone", dr["ShipperTel"].ToString());
                        cmd.Parameters.AddWithValue("@Destination", dr["Destination"].ToString());
                        cmd.Parameters.AddWithValue("@ConsigneeReference", dr["ConsigneeReference"].ToString());
                        cmd.Parameters.AddWithValue("@ConsigneeName", dr["ConsigneeName"].ToString());
                        cmd.Parameters.AddWithValue("@AttentionOf", dr["AttnOf"].ToString());
                        cmd.Parameters.AddWithValue("@ConsigneeAddress", dr["ConsigneeAddress"].ToString());
                        cmd.Parameters.AddWithValue("@DestinationCity", dr["DestCity"].ToString());
                        cmd.Parameters.AddWithValue("@DestinationState", dr["DestState"].ToString());
                        cmd.Parameters.AddWithValue("@DestinationZipCode", dr["DestZipCode"].ToString());
                        cmd.Parameters.AddWithValue("@DestinationCountry", dr["DestCountry"].ToString());
                        cmd.Parameters.AddWithValue("@ConsigneeNumber", dr["ConsigneeNumber"].ToString());
                        cmd.Parameters.AddWithValue("@Type", dr["Type"].ToString());
                        cmd.Parameters.AddWithValue("@PickupDate", dr["PickupDate"].ToString());
                        cmd.Parameters.AddWithValue("@Weight", dr["Weight"].ToString());
                        cmd.Parameters.AddWithValue("@ActualWeight", dr["ActualWeight"].ToString());
                        cmd.Parameters.AddWithValue("@Pieces", dr["Pieces"].ToString());
                        cmd.Parameters.AddWithValue("@WeightUnit", dr["Unit"].ToString());
                        cmd.Parameters.AddWithValue("@SourceID", dr["SourceID"].ToString());
                        cmd.Parameters.AddWithValue("@Route", dr["Route"].ToString());
                        cmd.Parameters.AddWithValue("@LineHauler", dr["LineHauler"].ToString());
                        cmd.Parameters.AddWithValue("@ForeignHAWBNumber", dr["ForeignHAWBNumber"].ToString());
                        cmd.Parameters.AddWithValue("@ShipperReference2", dr["ShipperReference2"].ToString());
                        cmd.Parameters.AddWithValue("@CommodityDescription", dr["CommodityDescription"].ToString());
                        cmd.Parameters.AddWithValue("@Reference1", dr["Reference1"].ToString());
                        cmd.Parameters.AddWithValue("@Reference2", dr["Reference2"].ToString());
                        cmd.Parameters.AddWithValue("@Reference3", dr["Reference3"].ToString());
                        cmd.Parameters.AddWithValue("@TotalAmount", dr["TotalAmount"].ToString());

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception exc)
                {
                    PostError("Row " + invcnt.ToString() + ":" + exc.Message);
                    InsertException(conn, InvoiceID, 1, invcnt, exc.Message);
                    conn.Close();
                }

            }

            conn.Close();
        }

        static void GetAramexCharges(SqlConnection conn, int InvoiceID, int NumberOfLines)
        {
            //Calls a webservice request for all items
            SqlCommand cmd = new SqlCommand();

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            int invcnt = 0;

            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM AramexInvoiceDetail WHERE InvoiceID=" + InvoiceID.ToString();

            SqlDataReader dr = cmd.ExecuteReader();

            //Get new connection
            SqlConnection cup = new SqlConnection(ConfigurationManager.ConnectionStrings["BILSIUpdate"].ConnectionString);
            cup.Open();

            //Update rate command
            SqlCommand uprate = new SqlCommand();
            uprate.CommandType = CommandType.StoredProcedure;
            uprate.CommandText = "dbo.uspAramexUpdateInvoiceDetail";
            uprate.Parameters.Add("@InvoiceDetailID", SqlDbType.Int);
            uprate.Parameters.Add("@ClientRateBase", SqlDbType.Money);
            uprate.Parameters.Add("@ClientRateTotal", SqlDbType.Money);
            uprate.Parameters.Add("@BilsiRateBase", SqlDbType.Money);
            uprate.Parameters.Add("@BilsiRateTotal", SqlDbType.Money);
            uprate.Connection = cup;

            while (dr.Read())
            {

                var reqinfo = new ShippingInfo();
                int detailid = Convert.ToInt32(dr["aidID"]);

                invcnt++;
                drawTextProgressBar("ID: " + detailid.ToString(), invcnt, NumberOfLines);

                reqinfo.UserName = "Shoplink";
                reqinfo.Password = "ShopLink.4050";
                reqinfo.ClientID = dr["aidShipperReference2"].ToString();
                reqinfo.ServiceType = Convert.ToInt32(dr["ServiceType"]);

                reqinfo.Address1 = dr["aidConsigneeAddress"].ToString();
                reqinfo.City = dr["aidDestinationCity"].ToString();

                reqinfo.ConsigneeName = dr["aidConsigneeName"].ToString();
                reqinfo.CountryCode = dr["aidDestinationCountry"].ToString();
                reqinfo.NumOfPackages = 1;

                reqinfo.PostalCode = dr["aidDestinationZipCode"].ToString();
                reqinfo.Province = dr["aidDestinationState"].ToString();
                reqinfo.ReferenceNumber = dr["aidShipperReference2"].ToString();

                if (dr["aidWeightUnit"].ToString() == "KG")
                {
                    reqinfo.TotalWeight = float.Parse(dr["aidWeight"].ToString()) * (float)2.204;
                }
                else
                {
                    reqinfo.TotalWeight = float.Parse(dr["aidWeight"].ToString());
                }

                reqinfo.OriginCity = dr["aidOriginCity"].ToString();
                reqinfo.OriginProvince = dr["aidOriginStateProv"].ToString();
                reqinfo.OriginCountryCode = dr["aidOriginCountry"].ToString();
                reqinfo.OriginPostalCode = dr["aidOriginZipCode"].ToString();

                reqinfo.RecordNumber = detailid;

                BILSIShipment.ShipmentRateResponse ordresp = new BILSIShipment.ShipmentRateResponse();

                //Get the aramex estimate
                try
                {
                    ordresp = GetShippingEstimate(reqinfo);
                }
                catch (Exception exc)
                {
                    InsertException(conn, InvoiceID, 6, detailid, exc.Message);
                }


                double clientratebase = 0;
                double clientratetotal = 0;

                if (ordresp.Error_Msg != null)
                {
                    foreach (var err in ordresp.Error_Msg)
                    {
                        PostError(detailid.ToString() + ": " + err.Error_Msg);
                        InsertException(conn, InvoiceID, 6, detailid, err.Error_Msg);
                    }
                }
                else
                {
                    if (ordresp.ShipmentRates != null)
                    {
                        clientratebase = ordresp.ShipmentRates[0].Base_Charge;
                        clientratetotal = ordresp.ShipmentRates[0].Total_Charge;
                    }
                }

                //Post additional charges
                if (ordresp.ShipmentRates != null)
                {

                    //Serialize the extra rate data so we can persist it in SQL
                    if (ordresp.ShipmentRates[0].Accessorial_Charges != null)
                    {
                        var chargexml = GetXMLFromObject(ordresp.ShipmentRates[0].Accessorial_Charges);

                        //Insert the records
                        InsertAccessorialCharges(conn, InvoiceID, detailid, 1, chargexml);  //Type 1 for Aramex 

                        //Get Bilsi Rate
                        BILSIShipment.ShipmentRateResponse Bilsicharge = new BILSIShipment.ShipmentRateResponse();

                        double bilsiratebase = 0;
                        double bilsiratetotal = 0;

                        try
                        {
                            Bilsicharge = GetBilsiRate(reqinfo);

                            if (Bilsicharge.ShipmentRates != null)
                            {
                                bilsiratebase = Bilsicharge.ShipmentRates[0].Base_Charge;
                                bilsiratetotal = Bilsicharge.ShipmentRates[0].Total_Charge;
                            }
                        }
                        catch (Exception exc)
                        {
                            InsertException(conn, InvoiceID, 6, detailid, "Bilsi Cost: " + exc.Message);
                        }

                        if (Bilsicharge.ShipmentRates == null)
                        {
                            InsertException(conn, InvoiceID, 6, detailid, "Bilsi cost not found");
                        }
                        else
                        {
                            //Update database with rates
                            uprate.Parameters["@InvoiceDetailID"].Value = detailid;
                            uprate.Parameters["@ClientRateBase"].Value = clientratebase;
                            uprate.Parameters["@ClientRateTotal"].Value = clientratetotal;
                            uprate.Parameters["@BilsiRateBase"].Value = bilsiratebase;
                            uprate.Parameters["@BilsiRateTotal"].Value = bilsiratetotal;
                            uprate.ExecuteNonQuery();

                            chargexml = GetXMLFromObject(Bilsicharge.ShipmentRates[0].Accessorial_Charges);

                            //Insert the records
                            InsertAccessorialCharges(conn, InvoiceID, detailid, 0, chargexml);  //Type 0 for Bilsi 
                        }
                    }
                }
            }
        }


        static void GetPurolatorCharges(SqlConnection conn, int InvoiceID)
        {
            //Calls a webservice request for all items
            SqlCommand cmd = new SqlCommand();

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM PurolatorInvoiceDetail WHERE REC_Type='S' AND InvoiceID=" + InvoiceID.ToString();

            SqlDataReader dr = cmd.ExecuteReader();

            //Get new connection
            SqlConnection cup = new SqlConnection(ConfigurationManager.ConnectionStrings["BILSIUpdate"].ConnectionString);
            cup.Open();

            //Update rate command
            SqlCommand uprate = new SqlCommand();
            uprate.CommandType = CommandType.Text;
            uprate.CommandText = "uspPurolatorUpdateInvoiceDetail";
            uprate.Parameters.Add("@InvoiceDetailID", SqlDbType.Int);
            uprate.Parameters.Add("@ClientRate", SqlDbType.Money);
            uprate.Parameters.Add("@BILSIRate", SqlDbType.Money);
            uprate.Connection = cup;

            while (dr.Read())
            {

                var reqinfo = new ShippingInfo();

                reqinfo.UserName = "ShopLink";
                reqinfo.Password = "ShopLink.4050";
                reqinfo.ClientID = dr["SHIP-TO_CITY"].ToString();

                reqinfo.Address1 = dr["SHIP-TO_ADDR"].ToString();
                reqinfo.City = dr["SHIP-TO_CITY"].ToString();

                reqinfo.ConsigneeName = dr["SHIP-TO_ATTN"].ToString();
                reqinfo.CountryCode = dr["SHIP-TO_COUNTRY"].ToString();
                reqinfo.NumOfPackages = 1;

                reqinfo.PostalCode = dr["SHIP-TO_POSTAL_CODE"].ToString();
                reqinfo.Province = dr["SHIP-TO_PROV"].ToString();
                reqinfo.ReferenceNumber = dr["CUSTOMER_REF1"].ToString();

                if (dr["DECLARED_WGT_UOM"].ToString() == "KG")
                {
                    reqinfo.TotalWeight = float.Parse(dr["DECLARED_WGT"].ToString()) * (float)2.24;
                }
                else
                {
                    reqinfo.TotalWeight = float.Parse(dr["DECLARED_WGT"].ToString());
                }

                reqinfo.OriginCity = dr["SHIP-FROM_CITY"].ToString();
                reqinfo.OriginProvince = dr["SHIP-FROM_PROV"].ToString();
                reqinfo.OriginCountryCode = dr["SHIP-FROM_COUNTRY"].ToString();
                reqinfo.OriginPostalCode = dr["SHIP-FROM_POSTAL_CODE"].ToString();

                reqinfo.ServiceType = int.Parse(dr["BILSIServiceType"].ToString());

                var ordresp = GetShippingEstimate(reqinfo);

                double clientcharge = 0;

                if (ordresp.Error_Msg != null)
                {
                    foreach (var err in ordresp.Error_Msg)
                    {
                        PostError(err.Error_Msg);
                    }
                }
                else
                {
                    clientcharge = ordresp.ShipmentRates[0].Total_Charge;
                    WriteMessage("Aramex Rate Found: " + clientcharge.ToString());
                }

                //Get Bilsi Rate
                var Bilsicharge = GetBilsiRate(reqinfo);

                //Update database with rate
                uprate.Parameters[1].Value = clientcharge;
                uprate.Parameters[2].Value = Bilsicharge;
                uprate.ExecuteNonQuery();

            }
        }


        static void GetCanadaPostCharges(SqlConnection conn, int InvoiceID, int NumberOfLines)
        {
            //Calls a webservice request for all items
            SqlCommand cmd = new SqlCommand();

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            int invcnt = 0;

            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM CanadaPostInvoiceDetail WHERE Record_Type = 'D' AND LEN(Order_Number_Returns)=0 AND InvoiceID=" + InvoiceID.ToString();

            SqlDataReader dr = cmd.ExecuteReader();

            //Get new connection
            SqlConnection cup = new SqlConnection(ConfigurationManager.ConnectionStrings["BILSIUpdate"].ConnectionString);
            cup.Open();

            //Update rate command
            SqlCommand uprate = new SqlCommand();
            uprate.CommandType = CommandType.StoredProcedure;
            uprate.CommandText = "dbo.uspCanadaPostUpdateInvoiceDetail";
            uprate.Parameters.Add("@InvoiceDetailID", SqlDbType.Int);
            uprate.Parameters.Add("@ClientRateBase", SqlDbType.Money);
            uprate.Parameters.Add("@ClientRateTotal", SqlDbType.Money);
            uprate.Parameters.Add("@BilsiRateBase", SqlDbType.Money);
            uprate.Parameters.Add("@BilsiRateTotal", SqlDbType.Money);
            uprate.Connection = cup;

            int detailid = 0;

            while (dr.Read())
            {

                detailid = Convert.ToInt32(dr["cpInvID"]);

                invcnt++;
                drawTextProgressBar("ID: " + detailid.ToString(), invcnt, NumberOfLines);

                var reqinfo = new ShippingInfo();

                // Confirm these are correct
                reqinfo.UserName = "12345";
                reqinfo.Password = "password";
                reqinfo.ClientID = "BILSI";

                reqinfo.Address1 = dr["Ship_To_Address"].ToString();
                reqinfo.City = dr["Ship_To_City"].ToString(); ;

                reqinfo.ConsigneeName = dr["Ship_To_Attn"].ToString();
                reqinfo.CountryCode = dr["Ship_To_Country"].ToString();
                reqinfo.NumOfPackages = 1;

                reqinfo.PostalCode = dr["Ship_To_Postal_Code"].ToString();
                reqinfo.Province = dr["Ship_To_Prov"].ToString();
                reqinfo.ReferenceNumber = dr["Customer_Ref1"].ToString();

                reqinfo.TotalWeight = float.Parse(dr["Declared_Weight"].ToString());

                reqinfo.OriginCity = dr["Ship_From_City"].ToString();
                reqinfo.OriginProvince = dr["Ship_From_Prov"].ToString();
                reqinfo.OriginCountryCode = dr["Ship_From_Country"].ToString();
                reqinfo.OriginPostalCode = dr["Ship_From_Postal_Code"].ToString();
                reqinfo.ServiceType = Convert.ToInt32(dr["ServiceType"]);

                reqinfo.RecordNumber = detailid;

                BILSIShipment.ShipmentRateResponse ordresp = new BILSIShipment.ShipmentRateResponse();

                //Get the Canada Post estimate
                try
                {
                    ordresp = GetShippingEstimate(reqinfo);
                }
                catch (Exception exc)
                {
                    InsertException(conn, InvoiceID, 6, detailid, exc.Message);
                }


                double clientratebase = 0;
                double clientratetotal = 0;

                if (ordresp.Error_Msg != null)
                {
                    foreach (var err in ordresp.Error_Msg)
                    {
                        PostError(detailid.ToString() + ": " + err.Error_Msg);
                        InsertException(conn, InvoiceID, 6, detailid, err.Error_Msg);
                    }
                }
                else
                {
                    if (ordresp.ShipmentRates != null)
                    {
                        clientratebase = ordresp.ShipmentRates[0].Base_Charge;
                        clientratetotal = ordresp.ShipmentRates[0].Total_Charge;
                    }
                }

                //Post additional charges
                if (ordresp.ShipmentRates != null)
                {

                    //Serialize the extra rate data so we can persist it in SQL
                    if (ordresp.ShipmentRates[0].Accessorial_Charges != null)
                    {
                        var chargexml = GetXMLFromObject(ordresp.ShipmentRates[0].Accessorial_Charges);

                        //Insert the records
                        InsertAccessorialCharges(conn, InvoiceID, detailid, 4, chargexml);  //Type 4 for Canada Post 

                        //Get Bilsi Rate
                        BILSIShipment.ShipmentRateResponse Bilsicharge = new BILSIShipment.ShipmentRateResponse();

                        double bilsiratebase = 0;
                        double bilsiratetotal = 0;

                        try
                        {
                            Bilsicharge = GetBilsiRate(reqinfo);

                            if (Bilsicharge.ShipmentRates != null)
                            {
                                bilsiratebase = Bilsicharge.ShipmentRates[0].Base_Charge;
                                bilsiratetotal = Bilsicharge.ShipmentRates[0].Total_Charge;
                            }
                        }
                        catch (Exception exc)
                        {
                            InsertException(conn, InvoiceID, 6, detailid, "Bilsi Rate: " + exc.Message);
                        }

                        if (Bilsicharge.ShipmentRates == null)
                        {
                            InsertException(conn, InvoiceID, 6, detailid, "Bilsi rate not found");
                        }
                        else
                        {
                            //Update database with rates
                            uprate.Parameters["@InvoiceDetailID"].Value = detailid;
                            uprate.Parameters["@ClientRateBase"].Value = clientratebase;
                            uprate.Parameters["@ClientRateTotal"].Value = clientratetotal;
                            uprate.Parameters["@BilsiRateBase"].Value = bilsiratebase;
                            uprate.Parameters["@BilsiRateTotal"].Value = bilsiratetotal;
                            uprate.ExecuteNonQuery();

                            chargexml = GetXMLFromObject(Bilsicharge.ShipmentRates[0].Accessorial_Charges);

                            //Insert the records
                            InsertAccessorialCharges(conn, InvoiceID, detailid, 0, chargexml);  //Type 0 for Bilsi 
                        }
                    }
                }
            }
        }

        static void GetFedExCharges(SqlConnection conn, int InvoiceID)
        {
            //Calls a webservice request for all items
            SqlCommand cmd = new SqlCommand();

            int cureccid = 0;

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM FedExInvoiceDetail WHERE InvoiceID=" + InvoiceID.ToString();

            SqlDataReader dr = cmd.ExecuteReader();

            //Update rate command
            SqlConnection upconn = new SqlConnection(ConfigurationManager.ConnectionStrings["BILSIUpdate"].ConnectionString);
            upconn.Open();

            SqlCommand uprate = new SqlCommand();
            uprate.CommandType = CommandType.Text;
            uprate.CommandText = "uspFedExUpdateInvoiceDetail";
            uprate.Parameters.Add("@InvoiceDetailID", SqlDbType.Int);
            uprate.Parameters.Add("@ClientRate", SqlDbType.Money);
            uprate.Parameters.Add("@BILSIRate", SqlDbType.Money);
            uprate.Connection = upconn;

            while (dr.Read())
            {

                cureccid = dr.GetInt32(0);

                uprate.Parameters[0].Value = cureccid;


                var reqinfo = new ShippingInfo();

                //Confirm 
                reqinfo.UserName = "12345";
                reqinfo.Password = "password";
                reqinfo.ClientID = "BILSI";

                reqinfo.Address1 = dr["Recipient_Address_1"].ToString();
                reqinfo.City = dr["Recipient_City"].ToString(); ;

                reqinfo.ConsigneeName = dr["Recipient_Company"].ToString();
                reqinfo.CountryCode = dr["Cntry2"].ToString();
                reqinfo.NumOfPackages = 1;

                //                         reqinfo.OrderNumber=
                //                               reqinfo.PhoneNumber =
                reqinfo.PostalCode = dr["Postal2"].ToString();
                reqinfo.Province = dr["ST2"].ToString();
                reqinfo.ReferenceNumber = dr["Ref_1"].ToString();


                //   if (dr["aidWeightUnit"].ToString() == "KG")
                //   {
                //       reqinfo.TotalWeight = float.Parse(dr["aidActualWeight"].ToString()) * (float)2.24;
                //  }
                // else
                // {
                reqinfo.TotalWeight = float.Parse(dr["Bill_Wt"].ToString());
                // }


                reqinfo.OriginCity = dr["Shipper_City"].ToString();
                reqinfo.OriginProvince = dr["ST"].ToString();
                reqinfo.OriginCountryCode = dr["Cntry1"].ToString();
                reqinfo.OriginPostalCode = dr["Postal"].ToString();

                reqinfo.ServiceType = 0; //Need to confirm value from [BILSIWeb].[dbo].[BASE_COURIER_SERVICE]

                var ordresp = GetShippingEstimate(reqinfo);

                double clientcharge = 0;

                if (ordresp.Error_Msg != null)
                {
                    foreach (var err in ordresp.Error_Msg)
                    {
                        PostError(err.Error_Msg);
                    }
                }
                else
                {
                    clientcharge = ordresp.ShipmentRates[0].Total_Charge;
                    WriteMessage("Aramex Rate Found: " + clientcharge.ToString());
                }

                //Get Bilsi Rate
                var Bilsicharge = GetBilsiRate(reqinfo);

                //Update database with rate
                uprate.Parameters[1].Value = clientcharge;
                uprate.Parameters[2].Value = Bilsicharge;
                uprate.ExecuteNonQuery();
            }
        }

        static void WriteMessage(string message)
        {
            string buffer = new string(' ', 80);
            Console.CursorLeft = 0;
            Console.WriteLine("» " + (message + buffer).Substring(0, 75));
            Console.BackgroundColor = ScreenBackColor;
        }

        static void PostError(string ErrorMessage)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            string buffer = new string(' ', 80);
            Console.CursorLeft = 0;
            Console.WriteLine(("► Error: " + (ErrorMessage + buffer).Substring(0, 70)));
            Console.BackgroundColor = ScreenBackColor;

            ExceptionCount = ExceptionCount + 1;
        }


        public static DataTable ReadCsv(string path)
        {
            DataTable result = new DataTable("CSVFile");

            try
            {
                using (TextFieldParser parser = new TextFieldParser(path))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    bool isFirstRow = true;
                    string cleanfield = "";
                    //IList<string> headers = new List<string>();

                    while (!parser.EndOfData)
                    {
                        string[] fields = parser.ReadFields();

                        //Increment duplicates (data has duplicate field names)
                        if (isFirstRow)
                        {
                            foreach (string field in fields)
                            {
                                //Fix up garbage data
                                //Remove leading trailing spaces

                                cleanfield = field.Trim();
                                cleanfield = cleanfield.Replace("  ", " ");
                                cleanfield = cleanfield.Replace("_ ", "_");
                                cleanfield = cleanfield.Replace(" ", "_");
                                cleanfield = cleanfield.Replace("-", "_");

                                if (result.Columns.Contains(cleanfield))
                                {
                                    result.Columns.Add(new DataColumn(cleanfield + result.Columns.Count.ToString(), typeof(string)));
                                }
                                else
                                {
                                    result.Columns.Add(new DataColumn(cleanfield, typeof(string)));
                                }
                            }
                            isFirstRow = false;
                        }
                        else
                        {
                            int i = 0;
                            DataRow row = result.NewRow();
                            foreach (string field in fields)
                            {
                                row[i++] = field;
                            }
                            result.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                PostError(exc.Message);
            }

            return result;
        }


        public static void GenerateCSV(DataTable dt, StringBuilder sb)
        {
            try
            {
                int count = 1;

                int totalColumns = dt.Columns.Count;
                /*
                foreach (DataColumn dr in dt.Columns)
                {
                    sb.Append(dr.ColumnName);

                    if (count != totalColumns)
                    {
                        sb.Append(",");
                    }

                    count++;
                }

                sb.AppendLine();
                */
                string value = String.Empty;
                foreach (DataRow dr in dt.Rows)
                {
                    for (int x = 0; x < totalColumns; x++)
                    {
                        value = dr[x].ToString();

                        if (value.Contains(",") || value.Contains("\""))
                        {
                            value = '"' + value.Replace("\"", "\"\"") + '"';
                        }

                        sb.Append(value);

                        if (x != (totalColumns - 1))
                        {
                            sb.Append(",");
                        }
                    }

                    sb.AppendLine();
                }
            }
            catch (Exception ex)
            {
                // Do something
            }

        }

        public static void drawTextProgressBar(string stepDescription, int progress, int total)
        {
            int totalChunks = 30;

            //draw empty progress bar
            Console.CursorLeft = 0;
            //Console.Write("["); //start
            Console.CursorLeft = totalChunks + 1;
            //Console.Write("]"); //end
            Console.CursorLeft = 1;

            double pctComplete = Convert.ToDouble(progress) / total;
            int numChunksComplete = Convert.ToInt16(totalChunks * pctComplete);

            //draw completed chunks
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write("".PadRight(numChunksComplete));

            //draw incomplete chunks
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Write("".PadRight(totalChunks - numChunksComplete));

            //draw totals
            Console.CursorLeft = totalChunks + 5;
            Console.BackgroundColor = ConsoleColor.Black;

            string output = progress.ToString() + " of " + total.ToString();
            Console.Write(output.PadRight(15) + stepDescription); //pad the output so when changing from 3 to 4 digits we avoid text shifting
        }


        private static BILSIShipment.ShipmentRateResponse GetBilsiRate(ShippingInfo requestInfo)
        {
            //Get Bilsi Rate
            requestInfo.UserName = BilsiUsername;
            requestInfo.Password = BilsiPassword;
            requestInfo.ClientID = BilsiClientID;

            var ordBilsi = GetShippingEstimate(requestInfo);

            if (ordBilsi.Error_Msg != null)
            {
                foreach (var err in ordBilsi.Error_Msg)
                {
                    PostError(requestInfo.RecordNumber.ToString() + ": " + err.Error_Msg);
                }

                return ordBilsi;
            }
            else
            {

                //Update database with rate
                //uprate.Parameters[1].Value = ordBilsi.Total_Charge;
                //uprate.ExecuteNonQuery();
                //  WriteMessage("Bilsi Rate Found: " + ordBilsi.ShipmentRates[0].Total_Charge.ToString());
                return ordBilsi;
            }
        }

        private static BILSIShipment.ShipmentRateResponse GetShippingEstimate(ShippingInfo requestInfo)
        {

            //Get the estimate
            //ordrequest.Body.request.Sender_Info.

            BILSIShipment.ShipmentRateRequest or = new BILSIShipment.ShipmentRateRequest();

            or.User_Name = requestInfo.UserName;
            or.Password = requestInfo.Password;
            or.Client_Id = requestInfo.ClientID;
            or.SignatureRequired = false;
            or.IsResidential = true;

            or.Receiver = new BILSIShipment.ReceiverInfo();

            or.Receiver.ReceiverId = "0";
            or.Receiver.City = requestInfo.City;
            or.Receiver.Country = requestInfo.CountryCode;
            or.Receiver.Postal_Zip_Code = requestInfo.PostalCode;
            or.Receiver.State_Province = requestInfo.Province;

            /*
            or.Consignee_Info.Name = requestInfo.ConsigneeName;
            or.Consignee_Info.Address_1 = requestInfo.Address1;
            or.Consignee_Info.Address_2 = requestInfo.Address2;
            or.Consignee_Info.City = requestInfo.City;
            or.Consignee_Info.State_Province = requestInfo.Province;
            or.Consignee_Info.Postal_Zip_Code = requestInfo.PostalCode;
            or.Consignee_Info.Country = requestInfo.CountryCode;
            or.Consignee_Info.Phone = requestInfo.PhoneNumber;
            or.Consignee_Info.ValidateAddressFlag = 0;
            */

            or.Sender = new BILSIShipment.SenderInfo();
            or.Sender.SenderId = 0;

            or.Sender.City = requestInfo.OriginCity;
            or.Sender.Country = requestInfo.OriginCountryCode;
            or.Sender.State_Province = requestInfo.OriginProvince;
            or.Sender.Postal_Zip_Code = requestInfo.OriginPostalCode;

            or.Shipment_Info = new BILSIShipment.ShipmentInfo();
            or.Shipment_Info.Wo_Type = "SMP";
            or.Shipment_Info.No_Packages = requestInfo.NumOfPackages;
            or.Shipment_Info.No_OverWeight_Packages = 0;
            or.Shipment_Info.Total_Weight = Math.Round(requestInfo.TotalWeight, 2);
            //or.ReferenceNumber = requestInfo.ReferenceNumber;
            or.ServiceType = requestInfo.ServiceType.ToString();
            or.SignatureRequired = false;
            or.DeclaredValue = 0;

            BILSIShipment.ShipmentRateServiceSoapClient seror = new BILSIShipment.ShipmentRateServiceSoapClient();
            BILSIShipment.ShipmentRateResponse oresp = new BILSIShipment.ShipmentRateResponse();

            try
            {
                oresp = seror.GetRate(or);
            }
            catch (Exception exc)
            {

                PostError(exc.Message);
            }

            return oresp;

        }

        private static void OutputAramexCSV(SqlConnection conn, int InvoiceID, bool resubexceptions)
        {
            //Get the output directory
            string outputdir = Properties.Settings.Default.UFOSOutputDirectory;
                     

            //Get the output datatable
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspAramexOutputCSV";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
            cmd.Parameters.AddWithValue("@SubmitExceptions", resubexceptions == true ? 1 : 0);
            SqlConnection con2 = new SqlConnection(conn.ConnectionString);
            con2.Open();

            cmd.Connection = con2;

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = cmd;

            DataSet ds = new DataSet();
            adapter.Fill(ds);

            StringBuilder sb = new StringBuilder();

            string shipid;
            string clientid;
            string filename;

            //Save each client to a separate CSV file
            foreach (DataTable dt in ds.Tables)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() == "TRAILER")
                    {
                        GenerateCSV(dt, sb);

                        clientid = dt.Rows[0][1].ToString();
                        shipid = dt.Rows[0][2].ToString();
                        if (resubexceptions)
                        {
                            filename = outputdir + @"\Repost_" + clientid + "_" + shipid + ".csv";
                        }
                        else
                        {
                            filename = outputdir + @"\Update_" + clientid + "_" + shipid + ".csv";
                        }

                        try
                        {
                            //Save the file
                            if (!File.Exists(filename))
                            {
                                // Create a file to write to.
                                File.WriteAllText(filename, sb.ToString());
                            }
                            else
                            {
                                File.Delete(filename);
                                File.WriteAllText(filename, sb.ToString());
                            }
                        }
                        catch(Exception exc)
                        {
                            PostError(exc.Message);
                        }

                        //Save duplicate file in a second location
                        outputdir = Properties.Settings.Default.OutputCopyDirectory;
                        filename = outputdir + @"\Update_" + clientid + "_" + shipid + ".csv";


                        //Save the duplicate file
                        try
                        {
                            if (!File.Exists(filename))
                            {
                                // Create a file to write to.
                                File.WriteAllText(filename, sb.ToString());
                            }
                            else
                            {
                                File.Delete(filename);
                                File.WriteAllText(filename, sb.ToString());
                            }
                        }
                        catch(Exception exc)
                        {
                            PostError(exc.Message);
                        }

                        WriteMessage("File saved: " + filename);

                        CompMessage.FilesCreated.Add(filename);

                        sb.Clear();
                    }
                    else
                    {
                        //Append the file
                        GenerateCSV(dt, sb);
                    }
                }
                else
                {
                    PostError("No data to post, check the exceptions");
                }
            }
        }


        private static void UpdateInvoiceType(SqlConnection conn, int InvoiceID, int InvoiceTypeID)
        {
            //Update the invoice status 
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspInvoiceUpdateType";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
            cmd.Parameters.AddWithValue("@InvoiceTypeID", InvoiceTypeID);
            cmd.Connection = conn;

            cmd.ExecuteNonQuery();

        }

        private static void UpdateInvoiceStatus(SqlConnection conn, int InvoiceID, int StatusID)
        {
            //Update the invoice status 
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspInvoiceUpdateStatus";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
            cmd.Parameters.AddWithValue("@StatusID", StatusID);

            SqlConnection conn2 = new SqlConnection(conn.ConnectionString);
            cmd.Connection = conn2;

            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }

            cmd.ExecuteNonQuery();

            conn2.Close();

        }

        private static void UpdateInvoiceNumber(SqlConnection conn, int InvoiceID, string InvoiceNumber)
        {
            //Update the invoice status 
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspInvoiceUpdateNumber";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
            cmd.Parameters.AddWithValue("@InvoiceNumber", InvoiceNumber);

            SqlConnection conn2 = new SqlConnection(conn.ConnectionString);
            cmd.Connection = conn2;

            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }

            cmd.ExecuteNonQuery();

            conn2.Close();

        }


        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }

        private static void InsertAccessorialCharges(SqlConnection conn, int InvoiceID, int ItemID, int RateType, string Charges)
        {
            //Update the invoice status 
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspInvoiceInsertAccessorialCharges";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
            cmd.Parameters.AddWithValue("@ItemID", ItemID);
            cmd.Parameters.AddWithValue("@RateType", RateType);
            cmd.Parameters.AddWithValue("@Charges", Charges);

            SqlConnection conn2 = new SqlConnection(conn.ConnectionString);
            cmd.Connection = conn2;

            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }

            cmd.ExecuteNonQuery();

            conn2.Close();

        }


        private static void InsertException(SqlConnection conn, int InvoiceID, int StatusID, int ItemID, string Message)
        {
            //Update the invoice status 
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspInvoiceExceptionInsert";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);
            cmd.Parameters.AddWithValue("@StatusID", StatusID);
            cmd.Parameters.AddWithValue("@ItemID", ItemID);
            cmd.Parameters.AddWithValue("@Exception", Message);

            SqlConnection conn2 = new SqlConnection(conn.ConnectionString);
            cmd.Connection = conn2;

            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }

            cmd.ExecuteNonQuery();

            conn2.Close();

        }

        private static void ProcessAramexDetails(SqlConnection conn, int InvoiceID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "uspAramexScanInvoiceDetail";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@InvoiceID", InvoiceID);

            SqlConnection conn2 = new SqlConnection(conn.ConnectionString);
            cmd.Connection = conn2;

            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }

            try
            {
                int exccnt = (int)cmd.ExecuteScalar();
                if (exccnt > 0)
                {
                    PostError("A total of " + exccnt.ToString() + " exceptions have been found. Please check Exception List");
                    ExceptionCount = exccnt - 1;
                }
            }
            catch (Exception exc)
            {
                InsertException(conn, InvoiceID, 12, -1, exc.Message);
            }
            finally
            {
                conn2.Close();
            }

        }

        private static bool GetIdleFile(string path)
        {
            var fileIdle = false;
            const int MaximumAttemptsAllowed = 30;
            var attemptsMade = 0;

            while (!fileIdle && attemptsMade <= MaximumAttemptsAllowed)
            {
                try
                {
                    using (File.Open(path, FileMode.Open, FileAccess.ReadWrite))
                    {
                        fileIdle = true;
                    }
                }
                catch
                {
                    attemptsMade++;
                    Thread.Sleep(100);
                }
            }

            return fileIdle;
        }


        private static void SendCompletionMessage(CompletionMessage CompMessage)
        {
            string redirecturl = Properties.Settings.Default.EditLaunchURL;

            SmtpClient client = new SmtpClient();


            MailAddress addressfrom = new MailAddress("bapservice@bilsi.com", "BAP Invoice Service");
            MailAddress addressto = new MailAddress(Properties.Settings.Default.CompletionEmailAddress);
            MailMessage message = new MailMessage(addressfrom, addressto);

            message.Subject = CompMessage.Subject;
            message.IsBodyHtml = true;

            string msgbody = "<p>An invoice file has been processed for Invoice: <b>" + CompMessage.InvoiceNumber + "</b></p>";
            msgbody = msgbody + "<p>A total of " + CompMessage.RecordsLoadedCount.ToString() + " invoice items have been processed</p>";
            msgbody = msgbody + "<h3>The following files have been copied to the UFOS directory for importing</h3>";
            msgbody = msgbody + "<ul>";
            foreach (string filename in CompMessage.FilesCreated)
            {
                msgbody = msgbody + "<li>" + filename.ToString() + "</li>";
            }

            msgbody = msgbody + "</ul>";

            if (CompMessage.ExceptionCount > 0)
            {
                msgbody = msgbody + "<h3>There were " + CompMessage.ExceptionCount.ToString() + " exceptions found</h3>";
                msgbody = msgbody + "<p>To review and correct the exception list, <a href='" + String.Format(redirecturl, CompMessage.InvoiceID.ToString()) + "'>Click Here</a>";
                /*     }
                     else
                     {
                         msgbody = msgbody + "<p>To review the invoice items, <a href='" + String.Format(redirecturl, CompMessage.InvoiceID.ToString(), "browse") + "'>Click Here</a>";
         
                 */
            }

            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Body = msgbody;

            client.Host = "mail.bh1.ca";
            client.Port = 25;
            client.UseDefaultCredentials = true;
            //client.Credentials 

            try
            {
                client.Send(message);
            }
            catch (Exception exc)
            {
                PostError(exc.Message);

            }
        }
    }
}
